package com.example.jenkins.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/jenkins")
    public String getPrebaJenkins(){
        return "Hola desde Jenkins";
    }
}
